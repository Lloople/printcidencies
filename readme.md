# PRINTcidències (Projecte Laravel 4.2)

Aquesta aplicació serveix per imprimir els controls de faltes i incidències setmanals de l'alumnat, tot fent servir la base de dades del djAu.

S'ha d'especificar on està aquesta base de dades a la configuració, ja que l'estirarà d'allà, no necessita base de dades propia.

Mitjançant un formulari on s'especifica la data, el grup i la franja horaria, podem filtrar quina setmana, quins alumnes i quin horari volem generar en PDF per ser descarregat a la nostra màquina.

L'aplicació està pensada per integrar un sistema de seguretat basat en els usuaris del djAu que pertanyin al grup "Direcció".

No descartem la possibilitat d'una opció per enviar el/s PDF generats al correu de la persona que ha iniciat sessió per ser impresos més tard, per si entra des d'un dispositiu mòbil. L'aplicació és totalment responsive.

djAu és un projecte de codi lliure per portar un control del centre d'estudis feta amb el llenguatje de programació Python 2.7 i el framework Django 1.6. Nosaltres no en som els propietaris.

## Requeriments

* Apache2, PHP5, MySQL server i client...
* Aplicació djAu funcionant
* Accès a la base de dades del djAu

## Fa servir

* HTML5
* CSS3
* PHP 5.4
* Twitter Bootstrap 3.2
* jQuery 1.11.2
* Plugin jQuery DatePicker
* Base de Dades del djAu
* Laravel Framework 4.2

# Informació Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing To Laravel

**All issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.**

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)