<?php

class SmsController extends BaseController {
    


    public function getChooseGroup() 
    {
        
        $data = array(
            'groups' => Grup::orderBy('descripcio_grup')->get()
        );

        return View::make('sms.group', $data);
    }
    
    public function getChooseStudent($groupId)
    {

        $group = Grup::find($groupId);
        $data = array(
            'students' => $group->alumnes
        );

        return View::make('sms.students', $data);
    }

    public function getSend($studentId)
    {
        $student = Alumne::find($studentId);

        $phones = $student->phones()->groupBy('telefon')->get();

        $data = array(
            'student' => $student,
            'phones' => $phones,
            'defaultSmsBody' => 'El seu fill/a ' . $student->nom . ' no ha assistit al càstig de Dimecres a la tarda.'
        );

        return View::make('sms.send', $data);
    }

    public function postSend()
    {

        $apiKey="";
        $test = "0";

        $sender = "Ins Muntaner"; 
        $numbers = Request::get('phone');
        $message = Request::get('body');
        $message = urlencode($message);
        $data = "apiKey=".$apiKey."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
        $ch = curl_init('http://api.txtlocal.com/send/?');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch); // Este es el resultado de la API
        curl_close($ch);

        $response = json_decode($result);

        if ($response->status == "success") {
            Request::session()->flash('message', 'SMS Enviat correctament');
        } else {
            Request::session()->flash('type', 'danger');
            Request::session()->flash('message', 'Ha fallat l\'enviament de SMS, contacta amb el departament d\'informàtica');

        }

        return Redirect::back();
    }


}
