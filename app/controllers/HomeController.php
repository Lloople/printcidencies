<?php

class HomeController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function getWelcome() {
        return View::make('hello');
    }
    
    public function getLogin() {
    
        return View::make('login');
        
    }
    
    
    public function getGeneraAdmin() {
        User::createAdmin();
        Session::flash('message', 'Admin creat correctament');
        Session::Flash('type', 'success');
        return Redirect::to('/');
    }
    
    /*
     * Comprovar les dades pasades per fer el login
     */
    public function postCheckMe() {
        $data = Input::all();
        if (Auth::attempt(array('username' => 'muntaneradmin', 'password' => $data['password']))) {
            Session::flash('message', 'Benvingut, administrador');
            return Redirect::to('');
        }
        else{
            Session::flash('message', 'Codi d\'accès incorrecte. ');
            Session::flash('type', 'danger');
            return Redirect::back();
        }
    }
    
    public function getLogout() {
        Auth::logout();
        Session::flash('message', 'Sessió tancada correctament');
        return Redirect::to('auth/login');
    }

}
