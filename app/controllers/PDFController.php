<?php

class PDFController extends BaseController {
    /*
     * Mostra el formulari només entrar a l'aplicació (Si hem iniciat sessió)
     */

    public function getIndex() {
        
        return View::make('homepage');
    }
    
    public function getImprimeixGrups() {
        $data['date'] = date('d-m-Y', strtotime("-1 week"));
        $data['grups'] = Grup::orderBy('descripcio_grup')->lists('descripcio_grup', 'id');
        $data['franjes_inici'] = Franja::lists('hora_inici', 'id');
        $data['franjes_fi'] = Franja::lists('hora_fi', 'id');
        return View::make('form-group', $data);
    }
    
    public function getImprimeixIndividual() {
        $alumnes = Alumne::whereNull('data_baixa')->orderBy('cognoms', 'asc')->orderBy('nom', 'asc')->get();
        $data['alumnes'] = array();
        foreach($alumnes as $al) {
            $data['alumnes'][$al->id] = $al->cognoms . ', ' . $al->nom;
        }
        return View::make('form-individual', $data);
    }
    
    

    public function getGeneraTest() {
        $alumnes = Alumne::all();
        echo '<meta charset="utf-8">';
        $pdf = '';
        foreach ($alumnes as $alumne) {
            $pdf .= $alumne->nom . ' ' . $alumne->cognoms . '<br/>';
        }
        return PDF::load($pdf, 'A4', 'landscape')->show();
    }

    public function postVeureIncidenciesGrup() {
        //@unlink(public_path() . '/storage/temp.pdf');
        $data = Input::all();
        echo '<meta charset="utf-8"/>';
        $info = array();

        /* ACONSEGUIR DILLUNS I DIUMENGE PER FER LA SELECT */

        $start = date($data['date']);                                       //Data passada per POST
        $start = date("Y-m-d", strtotime($start . " +1 days"));             //Data més un dia
        $start = date("Y-m-d", strtotime($start . " previous Monday"));     //Dilluns anterior a la data més un dia

        $firstday = $start;
        $franja_inici = $data['start'];
        $franja_fi = $data['end'];
        $total_franjes = $franja_fi - $franja_inici;
        $days = array('Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres');
        $finaldays = array();
        $final_frases = array();
        foreach ($days as $pos => $day) {
            $finaldays[$day] = date("Y-m-d", strtotime($firstday . " +" . $pos . " days"));
        }
        //Per cada grup que rebo per POST
        $final_print = array();
        $final_frases = array();
        //echo "<pre>";
        $groups = Grup::with(array('alumnes'=> function($q) {
            $q->orderBy('cognoms', 'ASC')->orderBy('nom', 'ASC');
        }))->whereIn('id', $data['group'])->get();
        foreach ($groups as $group) { //PER CADA GRUP
            
            //$group = Grup::find($group_id);
            //if (!$group) App::Abort(404);
            //$a_alumnes = Alumne::where('grup_id', $group_id)->orderBy('cognoms', 'ASC')->orderBy('nom', 'ASC')->get();
            
            $final_print[$group->descripcio_grup] = array();
            $final_frases[$group->descripcio_grup] = array();

            foreach ($group->alumnes as $al) { //PER CADA ALUMNE
                $final_print[$group->descripcio_grup][$al->nom . ' ' . $al->cognoms] = array();

                foreach ($finaldays as $day) { //PER CADA DIA
                    $final_print[$group->descripcio_grup][$al->nom . ' ' . $al->cognoms][$day] = array();

                    //$imparticions = Imparticio::where('dia_impartir', $day)->lists('id');
                    for ($n = $franja_inici; $n <= $franja_fi; $n++) { //PER CADA FRANJA
                        $letter = '&nbsp;';
                        $expulsio = Expulsio::where('dia_expulsio', $day)->where('alumne_id', $al->id)->where('franja_expulsio_id', $n)->first();
                        if ($expulsio) {
                            $letter = 'E';
                            $final_frases[$group->descripcio_grup][] = $this->addFrase($day, $n, $al, 'Ha estat expulsat de classe.'); //2.0
                        } else {
                            $incidencies = Incidencia::where('dia_incidencia', $day)->where('alumne_id', $al->id)->where('franja_incidencia_id', $n)->get();
                            $incidencia = $incidencies->first();
                            if ($incidencia) {
                                if ($incidencia->control_assistencia_id == null) //Si és una incidència feta a mà
                                    $letter = "O";
                                else {
                                    if ($incidencia->controlAssistencia->estat) {
                                        if ($incidencia->controlAssistencia->estat->codi_estat != "P") //Si no és una incidencia corretgida...
                                            $letter = $incidencia->controlAssistencia->estat->codi_estat;
                                        else // Temporal: Parche per incidències informatives i les rares que no sortien
                                            $letter= "O";
                                    }
                                    else {
                                        if ($incidencia->descripcio_incidencia != '')
                                            $letter = "O"; 
                                        else {
                                            $letter = "J"; //Justificades pel conserge, en Dani diu algo de permisos...
                                        }
                                    }
                                }
                                if ($letter != '&nbsp;') {
                                    foreach($incidencies as $inci) {
                                        $final_frases[$group->descripcio_grup][] = $this->addFrase($day, $n, $al, $inci->descripcio_incidencia);
                                    }       
                                }
                                    
                            }
                            else {
                                /* Agafar els CA que pertanyin al alumne que mirem i
                                 * que tinguin un impartir que correspongui al dia que mirem
                                 * i que aquest impartir tingui un horari que correspongui
                                 * a la franja que mirem
                                 */
                                
                                $ca = ControlAssistencia::where('alumne_id', $al->id)->whereHas('Impartir', function($q) use($day, $n) {
                                   $q->where('dia_impartir', $day)
                                           ->whereHas('Horari', function($e) use($n) {
                                              $e->where('hora_id', $n); 
                                           });
                                })->first();
                                if ($ca) {
                                    if (isset($ca->estat->codi_estat)) {
                                        if ($ca->estat->codi_estat != 'P')
                                                    $letter = $ca->estat->codi_estat;
                                    }
                                       
                                }
                            }
                        }
                        $final_print[$group->descripcio_grup][$al->nom . ' ' . $al->cognoms][$day][$n] = $letter; //Ultim pas del mega-hardcore-array
                        sort($final_frases[$group->descripcio_grup]);
                    }
                }
            }
        }
        $info['finaldays'] = $finaldays;
        $info['epic'] = $final_print;
        $info['timestart'] = $franja_inici;
        $info['timeend'] = $franja_fi;
        $info['frases'] = $final_frases;
        $html = View::make('base-pdf')->render();                       //Posar capçalera al document
        $html .= View::make('schedule', $info)->render();               //Posar una taula per cada grup demanat
        $html .= '</body></html>';
        //return PDF::loadHTML($html)->setPaper('a4')->setOrientation('landscape')->stream();
        echo $html;
    }
    
    public function postVeureIncidenciesIndividual() {
        $input = Input::all();
        $startdate = date('Y-m-d', strtotime($input['startdate']));
        $enddate = date('Y-m-d', strtotime($input['enddate']));
        $days = array($startdate, $enddate);
        
        $show_faltes    = false;
        $show_inc       = false;
        $show_exp       = false;
        
        

        if(isset($input['incidencies'])) {
            //41 resultats
            $incidencies = Incidencia::whereBetween('dia_incidencia', $days)
                    ->where('alumne_id', $input['alumne_id'])->get();
            $show_inc = true;
        }
        else
            $incidencies = array();
        
        if(isset($input['expulsions'])) {
            //4 resultats
            $expulsions = Expulsio::whereBetween('dia_expulsio', $days)
                    ->where('alumne_id', $input['alumne_id'])->get();
            $show_exp = true;
        }
        else
            $expulsions = array();
        
        if (isset($input['faltes'])) {
            //SELECT * FROM presencia_controlassistencia WHERE alumne_id = 515 AND estat_id IN (3, 4, 5) AND impartir_id IN (SELECT id FROM presencia_impartir WHERE dia_impartir BETWEEN '2014-09-12' AND '2014-10-30')
            //1 resultat
            $faltes = ControlAssistencia::with(array('impartir'=>function ($q) use($days) {
                $q->with(array('horari'=>function($o) {
                    $o->with(array('hora'=>function($e) {
                        $e->orderBy('hora_inici', 'asc');
                    }));
                }))->whereBetween('dia_impartir', $days)->orderBy('dia_impartir', 'asc');
            }))->where('alumne_id', $input['alumne_id'])->whereIn('estat_id', array(3, 4, 5))->get();
            $show_faltes = true;
        }
        else
            $faltes = array();
        
        /*
         * TODO:
         * -Agrupar les faltes per dia en un array com als productes de Snorkels
         * -Ordenar els dies i després les hores abans de agrupar-ho en l'array
         * -Fer el select escrivint a l'hora de triar un alumne
         */
        
        
        //$queries = DB::getQueryLog();
        //echo "<pre>";
        //dd($queries);
        $data['show_faltes'] = $show_faltes;
        $data['show_inc'] = $show_inc;
        $data['show_exp'] = $show_exp;
        $data['start'] = $startdate;
        $data['end'] = $enddate;
        $data['inc'] = $incidencies;
        $data['exp'] = $expulsions;
        $data['faltes'] = $faltes;
        $data['alumne'] = Alumne::find($input['alumne_id']);
        
        
        
        
        return View::make('print-individual', $data);
        
    }
    
    
    
    
    
    
    
    
    public static function addFrase($day, $n, $al, $text) {
        return $day . ' a les ' . Franja::find($n)->hora_inici . ': ' . $al->nom . ' ' . $al->cognoms . ': ' . $text;
    }

}
