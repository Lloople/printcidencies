@extends('base')

@section('css')
{{HTML::style('css/datepicker.css')}}
@stop
@section('js')
{{HTML::script('js/bootstrap-datepicker.js')}}
{{HTML::script('js/locales/bootstrap-datepicker.ca.js')}}
<script>
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            language: "ca",
            weekStart: 1,
            autoclose: true,
            todayHighlight: true

        });
        $('.datepicker-posticon').click(function() {
            $(this).prev().datepicker('show');
        });
    });
</script>
@stop

@section('content')
<h2 class='text-center'>Selecciona data d'inici, data de fi i alumne per mostrar la seva informació</h2>
<br>
{{Form::open(array('url'=>'veure-incidencies-individual', 'method'=>'POST'))}}
<div class='row'>
    <div class='col-lg-4'>
        <div class='form-box'>
            <div class='form-group'>
                <label>Data d'inici</label>
                <div class="input-group">
                    {{Form::text('startdate', date('d-m-Y'), array('class'=>'form-control datepicker', 'readonly'=>'readonly'))}}
                    <span class="input-group-addon datepicker-posticon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
                <label>Data fi</label>
                <div class="input-group">
                    {{Form::text('enddate', date('d-m-Y'), array('class'=>'form-control datepicker', 'readonly'=>'readonly'))}}
                    <span class="input-group-addon datepicker-posticon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </div>


        </div>
    </div>
    <div class='col-lg-4'>
        <div class="form-group">
            <label>Alumne</label>
            {{Form::select('alumne_id', $alumnes, Input::old('alumne_id'), array('class'=>'form-control'))}}
        </div>
    </div>
    <div class='col-lg-4'>
        <label>Dades a imprimir</label>
        <div class='form-group'>
            {{Form::checkbox('faltes', 1, true, array('id'=>'faltes'))}}
            {{Form::label('faltes', 'Faltes')}}
        </div>
        <div class='form-group'>
            {{Form::checkbox('incidencies', 1, true, array('id'=>'incidencies'))}} 
            {{Form::label('incidencies', 'Incidencies')}}
        </div>
        <div class='form-group'>
            {{Form::checkbox('expulsions', 2, true, array('id'=>'expulsions'))}} 
            {{Form::label('expulsions', 'Expulsions')}}
        </div>
        
    </div>
</div>
<br>
{{Form::submit('IMPRIMEIX', array('class'=>'btn btn-success btn-block'))}}
{{Form::close()}}
@stop