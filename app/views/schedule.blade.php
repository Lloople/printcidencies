
@foreach($epic as $group=>$alumnes)
<img class="img-header" src="{{URL::to('')}}/header.png" alt="capçalera"/>
<h3><span style="float: left;">GRUP: {{$group}}</span><span style="float: right;">Setmana del {{$finaldays['Dilluns']}}</span></h3>

<table class="main">
    <thead>
        <tr>
            <th style='border-right: 1px solid black; z-index: 9999;'>Alumne</th>
            @foreach($finaldays as $name=>$day) 
            <th style='border-right: 1px solid black; z-index: 9999;'>{{$day}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style='border-right: 1px solid black; z-index: 9999;'>Primera franja: {{Franja::find($timestart)->hora_inici}}</td>
            @foreach($finaldays as $name=>$day)
            <?php $position = 1; ?>
            <td style='border-right: 1px solid black; z-index: 9999;'>
                <table style='width: 100%;'>
                    <tr>
                @for($fran=$timestart; $fran<=$timeend;$fran++)
                <td class="franja">{{$position}}</td>
                <?php $position++; ?>
                @endfor
                    </tr>
                </table>
            </td>
            @endforeach
        </tr>
        @foreach($alumnes as $nom=>$dies)
        <tr>
            <td>{{$nom}}</td>
            @foreach($dies as $dia=>$franjes)
            <td style='border-right: 1px solid black; z-index: 9999;'>
                <table style="width: 100%;">
                    <tr>
                        @foreach($franjes as $franja=>$letter)
                        <td  class="franja franj-{{$fran}}">{{$letter}}</td>
                        @endforeach
                    </tr>
                </table>
                
            </td>
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
<div class="frases">
    @foreach($frases[$group] as $frase)
    <p class="frase">{{$frase}}<p>
    @endforeach
</div>
<div class="breaker"></div>
@endforeach












