@extends('base')

@section('css')
{{HTML::style('css/datepicker.css')}}
@stop
@section('js')
{{HTML::script('js/bootstrap-datepicker.js')}}
{{HTML::script('js/locales/bootstrap-datepicker.ca.js')}}
<script>
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            language: "ca",
            weekStart:1,
            autoclose: true,
            todayHighlight: true
            
        });
        $('.datepicker-posticon').click(function() {
            $(this).prev().datepicker('show');
        });
    });
</script>
@stop

@section('content')
<h2 class='text-center'>Selecciona la setmana, el grup, i l'interval de franjes a imprimir</h2>
<br>
{{Form::open(array('url'=>'veure-incidencies-grup', 'method'=>'POST'))}}
<div class='row form-select-date'>
    <div class='col-lg-offset-3 col-lg-3'>
        <div class='form-box'>
            <div class='form-group'>
                <label>Dia</label>
                <div class="input-group">
                    {{Form::text('date', $date, array('class'=>'form-control datepicker', 'readonly'=>'readonly'))}}
                    <span class="input-group-addon datepicker-posticon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label>Inici franja</label>
                {{Form::select('start', $franjes_inici, '2', array('class'=>'form-control'))}}
            </div>
            <div class="form-group">
                <label>Final franja</label>
                {{Form::select('end', $franjes_fi, '8', array('class'=>'form-control'))}}
            </div>
        </div>
    </div>
    <div class='col-lg-3'>
        <div class='form-box'>
            <div class="form-group">
                <label>Grups</label>
                {{Form::select('group[]', $grups, null, array('class'=>'form-control select-grups', 'multiple'=>'multiple'))}}
            </div>
        </div>
    </div>
</div>
<br>
{{Form::submit('IMPRIMEIX', array('class'=>'btn btn-success btn-block'))}}
{{Form::close()}}
@stop