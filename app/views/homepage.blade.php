@extends('base')

@section('content')

<div class='row'>
    <div class='col-md-6'>
        <a href="{{URL::to('imprimeix-grups')}}" class="btn btn-block btn-info btn-lg">IMPRIMIR GRUPS</a>
    </div>
    <div class='col-md-6'>
        <a href="{{URL::to('imprimeix-individual')}}" class="btn btn-block btn-info btn-lg">IMPRIMIR INDIVIDUAL</a>
    </div>
    <div class="clearfix"></div>
    <div class='col-md-6'>
        <a href="{{URL::to('sms/choose-group')}}" class="btn btn-block btn-info btn-lg">ENVIAR SMS</a>
    </div>
</div>
@stop