@extends('base')

@section('content')
<div class='row'>
    <div class="col-lg-offset-4 col-lg-4">
        <div class="well">
        {{Form::open(array('url'=>'auth/check-me', 'method'=>'POST'))}}
        <label>Escriu el codi d'accès per entrar a l'aplicació</label>
        {{Form::password('password', array('class'=>'form-control'))}}
        <br>
        {{Form::submit('ENTRA', array('class'=>'btn btn-success btn-block'))}}
        {{Form::close()}}
        </div>
    </div>
</div>
@stop