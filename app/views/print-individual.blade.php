<!DOCTYPE html>
<html>
    <head>
        <meta charset='UTF-8'>
        <style>
            * {
                font-family: arial;
                font-size: 25px;
            }
            .quadre {
                text-align: center;
                margin: 0 auto;
                width: 700px;
                font-size: 30px;
                display: block;
            }
            
            .quadre .quad {
                line-height: 50px;
                height: 100px;
                text-align:center;
                border: 1px solid black;
                padding: 1px;
                width:200px;
                float: left;
                margin: 0 10px;
            }
            .quadre .quad span {
                font-weight: bold;
            }
            ul li{
                list-style: none;
            }
            
            
        </style>
    </head>
    <body>
        <img class="img-header" src="{{URL::to('')}}/header.png" alt="capçalera"/>
        <h2>{{$alumne->cognoms}}, {{$alumne->nom}}</h2>
        <h3>{{$alumne->grup->curs->nom_curs_complert}} {{$alumne->grup->nom_grup}}</h3>
        <h4>Data inici: {{date('d-m-Y', strtotime($start))}}</h4>
        <h4>Data fi: {{date('d-m-Y', strtotime($end))}}</h4>
        <div class='quadre'>
            @if($show_faltes)
            <div class="quad">
                Faltes
                <br>
                <span>{{count($faltes)}}</span>
            </div>
            @endif
            @if($show_inc)
            <div class="quad">
                Incidències
                <br>
                <span>{{count($inc)}}</span>
            </div>
            @endif
            @if($show_exp)
            <div class="quad">
                Expulsions
                <br>
                <span>{{count($exp)}}</span>
            </div>
            @endif
        </div>
        <div style="float: none; clear: both; width: 100%; height: 1px;">&nbsp;</div>
        <div class="content">
            @if(count($faltes))
            <h2>Faltes</h2>
            <ul class="faltes">
                @foreach($faltes as $falta)
                <li>{{date('d-m-Y', strtotime($falta->impartir->dia_impartir))}} a les {{date('H:i', strtotime($falta->impartir->horari->hora->hora_inici))}}</li>
                @endforeach
            </ul>
            @endif
            @if(count($inc))
            <h2>Incidències</h2>
            <ul class="incidencies">
                @foreach($inc as $incidencia)
                <li>{{date('d-m-Y', strtotime($incidencia->dia_incidencia))}} a les {{date('H:i', strtotime($incidencia->FranjaIncidencia->hora_inici))}}: {{$incidencia->descripcio_incidencia}}</li>
                @endforeach
            </ul>
            @endif
            @if(count($exp))
            <h2>Expulsions</h2>
            <ul class="incidencies">
                @foreach($exp as $expulsio)
                <li>{{date('d-m-Y', strtotime($expulsio->dia_expulsio))}} a les {{date('H:i', strtotime($expulsio->FranjaExpulsio->hora_inici))}}: {{$expulsio->motiu_expulsio}}</li>
                @endforeach
            </ul>
            @endif
        </div>
    </body>
</html>




