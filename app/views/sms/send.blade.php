@extends('base')

@section('content')

<div class="container">
<div class="row">
	<div class="col-md-offset-2 col-md-8">
		<h1>Enviar SMS als pares de: <br>{{ $student->nom . ' ' . $student->cognoms }}</h1>

		<form method="POST" action="{{ url('sms/send') }}">
		<label for="phone" class="control-label">Escull a quin número enviar el SMS</label>
		<select name="phone" class="form-control">
		@foreach($phones as $phone)
			<option value="{{ $phone->telefon }}">{{ $phone->telefon }}</option>
		@endforeach		
		</select>
		<label for="body" class="control-label">Escrigui el contingut del SMS que rebrà el pare de l'alumne.</label>
		<textarea name="body" class="form-control">{{ $defaultSmsBody }}</textarea>
		<br><br><br>
		<button class="btn btn-info text-lg pull-right">Enviar SMS</button>
		</form>

	</div>
</div>

</div>
@endsection