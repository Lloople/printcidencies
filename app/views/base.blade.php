<html lang="ca">
    <head>
        <meta charset="UTF-8">
        <title>PrintCidències</title>
        {{HTML::style('css/bootstrap.css')}}
        {{HTML::style('css/style.css')}}
        {{HTML::script('js/jquery.min.js')}}
        {{HTML::script('js/bootstrap.min.js')}}
        @yield('css')
        
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{URL::to('/')}}">PRINTCIDENCIES</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @if(Auth::check())
                        <li><a href="{{URL::to('auth/logout')}}">TANCA LA SESSIÓ</a></li>
                        @endif
                        
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container" style="margin-top: 70px;">
            @if (Session::has('message'))
            <?php
            if (Session::has('type')) $type = Session::get('type');
            else $type = "success";
            ?>
            <div class='alert text-center alert-{{$type}}'>
                <p>{{Session::get('message')}}</p>
            </div>
            @endif
            @yield('content')
        </div>
        @yield('js')
    </body>
</html>