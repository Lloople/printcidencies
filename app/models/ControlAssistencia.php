<?php

class ControlAssistencia extends Eloquent  {
    protected $table = 'presencia_controlassistencia';
    
    public function estat() {
        return $this->belongsTo("Estat");
    }
    public function impartir() {
        return $this->belongsTo("Imparticio");
    }
}

