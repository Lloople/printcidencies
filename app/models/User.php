<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'laravel_users';
        public $timestamps = false;

        /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
        
        
        public static function createAdmin() {
            $admin = User::where('username', 'muntaneradmin')->delete();
            $user = new User();
            $user->username = 'muntaneradmin';
            $user->password = Hash::make('1nsmunt4n3r2014');
            $user->email = 'info@insmuntaner.com';
            $user->is_admin = true;
            $user->save();
            
        }

}
