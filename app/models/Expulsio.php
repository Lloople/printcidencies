<?php

class Expulsio extends Eloquent  {
    protected $table = 'incidencies_expulsio';
    
    
    public function alumne() {
        return $this->belongsTo("Alumne");
    }
    
    public function controlAssistencia() {
        return $this->belongsTo("ControlAssistencia");
    } 
    
    public function FranjaExpulsio() {
        return $this->belongsTo("Franja");
    }
}

