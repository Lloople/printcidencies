<?php

class Incidencia extends Eloquent  {
    protected $table = 'incidencies_incidencia';
    
    
    public function alumne() {
        return $this->belongsTo("Alumne");
    }
    
    public function professor() {
        return $this->belongsTo("Professor");
    }
    public function controlAssistencia() {
        return $this->belongsTo("ControlAssistencia");
    } 
    
    public function FranjaIncidencia() {
        return $this->belongsTo("Franja");
    }
}

