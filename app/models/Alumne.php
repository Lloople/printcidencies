<?php

class Alumne extends Eloquent  {
    protected $table = 'alumnes_alumne';
    
    
    public function grup() {
        return $this->belongsTo("Grup");
    }

    public function phones()
    {
    	return $this->hasMany('Phone', 'alumne_id');
    }
}

