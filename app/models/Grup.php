<?php

class Grup extends Eloquent  {
    protected $table = 'alumnes_grup';
    
    public function alumnes() {
        return $this->hasMany('Alumne');
    }
    
    public function curs() {
        return $this->belongsTo('Curs');
    }
    
}

