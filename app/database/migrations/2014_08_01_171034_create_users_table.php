<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('laravel_users', function($table) {
           $table->increments('id');
           $table->string('username', 100);
           $table->string('password', 255);
           $table->string('email');
           $table->boolean('is_admin');
           $table->rememberToken();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('laravel_users');
    }

}
